<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(["guest:admin"])->get('/', function () {
    return view('auth.login');
});

// Route::get('/', 'HomeController@inde')->name("test1");


Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/dashboard', 'HomeController@dashboard')->name("dashboard");
    Route::resource('companies', 'CompanyController');
    Route::post('companies/{company}/delete-logo', "CompanyController@deleteLogo")->name("companies.delete-logo");
    Route::resource('employees', 'EmployeeController');
});
