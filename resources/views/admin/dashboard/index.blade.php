@extends('admin.layouts.app')
@section("content")


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- /.content-header -->
<!-- Main content -->
{{-- <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-success text-white" href="{{ route('admin.useful-links.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
                        </div>
                    </div>
                </div>
                @include('admin.layouts.includes.notification')
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered table-hover" id="link-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th nowrap>Name</th>
                                    <th>Sequence</th>
                                    <th>Published Status</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($usefulLinks ?? [] as $key => $usefulLink)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td nowrap>
                                        <b>English Name</b> : {{ $usefulLink->name_en }}<br/>
                                        <b>Chinese Name</b> : {{ $usefulLink->name_cn ?? '-' }}
                                    </td>
                                    <td>{{ $usefulLink->sequence }}</td>
                                    <td nowrap>
                                        <label class="text-{{ getUsefulLinkPublishedStatusColorClass($usefulLink->publish_status) }}">{{ $usefulLink->publish_status_text ?? '-'}}</label>
                                        <a href="{{ $usefulLink->link }}" target="_blank"><i class="fas fa-link"></i></a>
                                    </td>
                                    <td nowrap>
                                        @if($usefulLink->updated_at)
                                        <b>Updated Date</b> : {{ date_format(date_create($usefulLink->updated_at), 'Y-m-d h:i A') }}
                                        @else
                                        <b>Created Date</b> : {{ date_format(date_create($usefulLink->created_at), 'Y-m-d h:i A') }}
                                        @endif
                                    <td nowrap>
                                        <a class="btn btn-primary" href="{{ route('admin.useful-links.show', $usefulLink) }}"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-warning" href="{{ route('admin.useful-links.edit', $usefulLink) }}"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-danger delete-btn" data-href="#">
                                            <i class="fa fa-trash"></i>
                                            <form action="{{ route('admin.useful-links.destroy', $usefulLink) }}" method="POST" class="d-none">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection

@section('script')
{{-- <script type="text/javascript">
    $(document).ready(function () {
        $('#link-table').DataTable({
            // "paging": true,
            // "lengthChange": false,
            // "searching": false,
            // "ordering": true,
            // "info": true,
            "autoWidth": true,
            // "responsive": true,
        });
    })
</script> --}}
@endsection