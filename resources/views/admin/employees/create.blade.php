@extends('admin.layouts.app')

@section("content")
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create Employee</h1>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.employees.index') }}">Employees</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-primary text-white" href="{{ route('admin.employees.index') }}"><i class="fa fa-list"></i> Index</a>
                        </div>
                    </div>
                    @include('admin.layouts.includes.notification')
                    <form action="{{ route('admin.employees.store') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="card card-primary">
                            <div class="card-header bg-gray">
                                <b class="card-title"><i class="fas fa-users"></i> Employee Information</b>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">First Name* :</label>
                                            <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" placeholder="Enter first name..." value="{{ old('first_name') }}" required>
                                            @error('first_name')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Last Name* :</label>
                                            <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" placeholder="Enter last name..." value="{{ old('last_name') }}" required>
                                            @error('last_name')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Company :</label>
                                            <select name="company" class="form-control @error('company') is-invalid @enderror">
                                                <option value>Select company</option>
                                                @foreach ($companies as $company)
                                                    <option value="{{ $company['id'] }}" {{ $company['id'] == old('company') ? 'selected' : '' }}>{{ $company['name'] }}</option>
                                                @endforeach
                                            </select>
                                            @error('company')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Email :</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter email..." value="{{ old('email') }}">
                                            @error('email')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Mobile No :</label>
                                            <input type="text" class="form-control @error('mobile_no') is-invalid @enderror" name="mobile_no" placeholder="Enter mobile no..." value="{{ old('mobile_no') }}">
                                            @error('mobile_no')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success float-right"><i class="fa fa-plus"></i> Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
