@extends('admin.layouts.app')
@section("content")


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Employees</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Employees</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-success text-white" href="{{ route('admin.employees.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
                        </div>
                    </div>
                </div>
                @include('admin.layouts.includes.notification')
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered table-hover" id="company-table">
                            <thead>
                                <tr class="table-success">
                                    <th>No</th>
                                    <th nowrap>Full Name</th>
                                    <th>Company</th>
                                    <th nowrap>Email</th>
                                    <th>Mobile No</th>
                                    <th nowrap>Created Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees ?? [] as $key => $employee)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $employee->full_name }}</td>
                                    <td>
                                        @if($employee->company)
                                        <a href="{{ route('admin.companies.show', $employee->company) }}">{{ $employee->company->name }}</a>
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td nowrap>
                                        @if($employee->email)
                                        <a href="mailto:{{$employee->email}}">{{$employee->email}}</a>                                            
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td>
                                        @if($employee->mobile_no)
                                        <a href="tel:{{$employee->mobile_no}}">{{$employee->mobile_no}}</a>                                            
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td nowrap>{{ date_format(date_create($employee->created_at), 'Y-m-d h:i A') }}</td>
                                    <td nowrap>
                                        <a class="btn btn-primary" href="{{ route('admin.employees.show', $employee) }}"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-warning" href="{{ route('admin.employees.edit', $employee) }}"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-danger delete-btn" data-href="#">
                                            <i class="fa fa-trash"></i>
                                            <form action="{{ route('admin.employees.destroy', $employee) }}" method="POST" class="d-none">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#company-table').DataTable({
            // "paging": true,
            // "lengthChange": false,
            // "searching": false,
            // "ordering": true,
            // "info": true,
            "autoWidth": true,
            // "responsive": true,
        });
    })
</script>
@endsection