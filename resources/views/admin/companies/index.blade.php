@extends('admin.layouts.app')
@section("content")


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Companies</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Companies</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-success text-white" href="{{ route('admin.companies.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
                        </div>
                    </div>
                </div>
                @include('admin.layouts.includes.notification')
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered table-hover" id="company-table">
                            <thead>
                                <tr class="table-success">
                                    <th>No</th>
                                    <th nowrap>Company Name</th>
                                    <th nowrap>Email</th>
                                    <th>Logo</th>
                                    <th>Website</th>
                                    <th nowrap>Created Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($companies ?? [] as $key => $company)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $company->name }}</td>
                                    <td>
                                        @if($company->email)
                                        <a href="mailto:{{$company->email}}">{{$company->email}}</a>                                            
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td>
                                        @if($company->logo_link != '')
                                        <img src="{{ asset($company->logo_link) }}" width="100" alt="Company Logo">
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td>
                                        @if($company->website)
                                        <a href="{{$company->website}}" target="_blank">{{$company->website}}</a>                                            
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td nowrap>{{ date_format(date_create($company->created_at), 'Y-m-d h:i A') }}</td>
                                    <td nowrap>
                                        <a class="btn btn-primary" href="{{ route('admin.companies.show', $company) }}"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-warning" href="{{ route('admin.companies.edit', $company) }}"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-danger delete-btn" data-href="#" data-type="company">
                                            <i class="fa fa-trash"></i>
                                            <form action="{{ route('admin.companies.destroy', $company) }}" method="POST" class="d-none">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        // $('#company-table').DataTable({
        //     // "paging": true,
        //     // "lengthChange": false,
        //     // "searching": false,
        //     // "ordering": true,
        //     // "info": true,
        //     "autoWidth": true,
        //     // "responsive": true,
        // });
    })
</script>
@endsection