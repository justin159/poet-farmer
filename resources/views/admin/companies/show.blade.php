@extends('admin.layouts.app')
@section("content")

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Show Company</h1>
                    </div>
                    <div class="col-sm-6">          
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('admin.companies.index')}}">Companies</a></li>
                            <li class="breadcrumb-item active">Show</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 text-right">
                                <div class="form-group">
                                    <a class="btn btn-primary text-white" href="{{ route('admin.companies.index') }}"><i class="fa fa-list" aria-hidden="true"></i> Index</a>
                                    <a class="btn btn-warning text-white" href="{{ route('admin.companies.edit', $company) }}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
                                    <a class="btn btn-danger text-white delete-btn" data-type="company">
                                        <i class="fa fa-trash" aria-hidden="true"></i> 
                                        <form action="{{ route('admin.companies.destroy', $company) }}" method="POST" class="d-none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        Delete
                                    </a>
                                </div>
                            </div>
                        </div>
                        @include('admin.layouts.includes.notification')
                        <div class="card card-primary">
                            <div class="card-header bg-gray">
                                <b class="card-title"><i class="fas fa-building"></i> Company Information</b>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Company Name :</label>
                                            <p class="form-control-static">{{ $company->name ?? '-' }}</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Email :</label>
                                            <p class="form-control-static">
                                                @if($company->email)
                                                <a href="mailto:{{$company->email}}">{{$company->email}}</a>                                            
                                                @else
                                                -
                                                @endif    
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Logo :</label>
                                            <p class="form-control-static">
                                                @if($company->logo_link)
                                                <img src="{{ asset($company->logo_link) }}" width="80%" alt="Company Logo">
                                                @else
                                                -
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Website :</label>
                                            <p class="form-control-static">
                                                @if($company->website)
                                                <a href="{{$company->website}}" target="_blank">{{$company->website}}</a>                                            
                                                @else
                                                -
                                                @endif    
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
