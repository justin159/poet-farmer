@extends('admin.layouts.app')

@section("content")
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create Company</h1>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.companies.index') }}">Companies</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-primary text-white" href="{{ route('admin.companies.index') }}"><i class="fa fa-list"></i> Index</a>
                        </div>
                    </div>
                    @include('admin.layouts.includes.notification')
                    <form action="{{ route('admin.companies.store') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="card card-primary">
                            <div class="card-header bg-gray">
                                <b class="card-title"><i class="fas fa-building"></i> Company Information</b>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Name* :</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter name..." value="{{ old('name') }}" required>
                                            @error('name')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Email :</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter email..." value="{{ old('email') }}">
                                            @error('email')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label for="logo">Logo :</label>
                                            <input type="file" accept="image/*" class="form-control @error('logo') is-invalid @enderror" name="logo" value="{{ old('logo') }}">
                                            <small>Recommend to upload <b>jpeg/jpg or png</b> image format; <br>size recommended to <b>minumum 100px x 100px</b></small>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Website :</label>
                                            <input type="text" class="form-control @error('website') is-invalid @enderror" name="website" placeholder="Enter website..." value="{{ old('website') }}">
                                            @error('website')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success float-right"><i class="fa fa-plus"></i> Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
