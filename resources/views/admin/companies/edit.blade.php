@extends('admin.layouts.app')

@section("content")
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Edit Company</h1>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-primary text-white" href="{{ route('admin.companies.index') }}"><i class="fa fa-list"></i> Index</a>
                            <a class="btn btn-warning" href="{{ route('admin.companies.show', $company) }}"><i class="fa fa-eye"></i> View</a>
                        </div>
                    </div>
                    @include('admin.layouts.includes.notification')
                    <form action="{{ route('admin.companies.update', $company) }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="card card-primary">
                            <div class="card-header bg-gray">
                                <b class="card-title"><i class="fas fa-building"></i> Company Information</b>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Name* :</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter name..." value="{{ old('name_en', $company->name) }}" required>
                                            @error('name')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Email :</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter email..." value="{{ old('email', $company->email) }}">
                                            @error('email')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label for="logo">Logo :</label>
                                            @if($company->logo_link)
                                                <input type="hidden" name="logo" value="{{ $company->logo_link }}">
                                            @endif
                                            <input type="file" accept="image/*" class="form-control @error('logo') is-invalid @enderror" name="logo" value="{{ old('logo') }}">
                                            <small>Recommend to upload <b>jpeg/jpg or png</b> image format; <br>size recommended to <b>minumum 100px x 100px</b></small>
                                            <br>

                                            @if($company->logo_link)
                                            <label for="floor_plan" class="mt-3">Current Logo :</label><br>
                                            <img src="{{ asset($company->logo_link) }}" alt="" width="320px">
                                            <a class="btn btn-danger text-white delete-logo-btn">
                                                <i class="fa fa-trash" aria-hidden="true"></i> 
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="control-label">Website :</label>
                                            <input type="text" class="form-control @error('website') is-invalid @enderror" name="website" placeholder="Enter website..." value="{{ old('website', $company->website) }}">
                                            @error('website')
                                            <span class="invalid-feedback d-block" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-pencil-alt"></i> Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).on('click', '.delete-logo-btn', function (event) {
            event.preventDefault();

            var that = $(this);
            let url = "{{ route('admin.companies.delete-logo', 'placeholder') }}";
            url = url.replace('placeholder', '{{ $company->id }}');

            Swal.fire({
                title: 'Are you sure to delete this logo?',
                icon: 'warning',
                text: 'You won\'t be able to undo this action.',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: "post",
                        dataType: 'json',
                        data: {
                            '_token': '{{ csrf_token() }}',
                        },
                        success: function(response){
                            if (response.success) {
                                location.reload();
                            } else {
                                console.log(response.message);
                            }
                        },
                        error: function(data){
                            console.log("No service available");
                        }
                    });
                }
            })
        });
    </script>
@endsection

