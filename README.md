## Importing Poet Farmer Assessment

My end using xampp with virtualhost, therefore following steps will be based on xampp

1. Clone the project by using either SSH or HTTPS method
2. Run composer install command
3. Run php artisan key:generate command
4. Run xampp and create db table named 'poetfarmer' in localhost
5. Run php artisan migrate command
6. Run the project by start apache service
7. If wanted to have preloaded data for companies and employees, can run seeder for both (php artisan db:seed --class=CompanySeeder, php artisan db:seed --class=EmployeeSeeder)

## Login
Login into the Admin Panel with following details
Email: admin@admin.com
Password: password

## Create & Edit Company
User able to create a company with the conditions below
- Name must not be empty
- Email and Website (link) are optional to fill
- Email must be in email format and unique for each company
- Website must be in url format
- Logo can be optional to fill, it must be in image file and minimum of 100 x 100 in size

## View Company
User able to view a company in detail

## Delete Company
User able to delete a company
- User able to delete the company logo separately
- Any employee assigned under the selected company will be auto unassigned

## Create & Edit Employee
User able to create an employee with the conditions below
- First Name and Last Name must not be empty
- Company Selection, Email, and Mobile No are optional to fill
- Email must be in email format and unique for each employee
- Contact No must be unique for each employee

## View Employee
User able to view an employee in detail

## Delete Employee
User able to delete an employee

 
