<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Employee;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'              => ['required', 'string'],
            'last_name'               => ['required', 'string'],
            'company'                 => ['nullable'],
            'email'                   => ['nullable', 'email', Rule::unique('employees')->ignore($this->route('employee'), 'id')],
            'mobile_no'               => ['nullable', Rule::unique('employees')->ignore($this->route('employee'), 'id')],
        ];
    }
}
