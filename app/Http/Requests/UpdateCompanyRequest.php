<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\UniqueEmail;
use App\Models\Company;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                    => ['required', 'string'],
            'email'                   => ['nullable', 'email', Rule::unique('companies')->ignore($this->route('company'), 'id')],
            'logo'                    => ['nullable', 'file', 'image', 'dimensions:min_width=100,min_height=100'],
            'website'                 => ['nullable', 'url'],
        ];
    }
}
