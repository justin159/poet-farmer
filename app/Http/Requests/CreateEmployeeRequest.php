<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UniqueEmail;
use App\Rules\UniqueMobileNo;
use App\Models\Employee;

class CreateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'              => ['required', 'string'],
            'last_name'               => ['required', 'string'],
            'company'                 => ['nullable'],
            'email'                   => ['nullable', 'email', new UniqueEmail(new Employee)],
            'mobile_no'               => ['nullable', 'string', new UniqueMobileNo(new Employee)],
        ];
    }
}
