<?php

namespace App\Http\Controllers\Admin;

use Log;
use Session;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\Company;
use App\Http\Requests\CreateCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::latest()->get();
        return view('admin.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCompanyRequest $request)
    {
        try {
            $company = Company::create([
                'name'  => $request->name,
                'email' => $request->email,
                'website'  => $request->website,
            ]);

            if ($request->hasFile('logo')) {
                $uploadPath = "assets/web/images/companies/$company->id/";
                $file      = $request->file('logo');
                $fileName = $request->logo->getClientOriginalName();
                $uploadSuccess = $file->move($uploadPath, $fileName); // uploading file to given path
                if (!$uploadSuccess) {
                    return redirect()->route("admin.companies.create")->with("failure", "Error when tried to save company logo.")->withInput();
                }

                $imageLink = $uploadPath . $fileName;
                $company->update([
                    'logo_link' => asset($imageLink),
                ]);
            };

            Session::flash('success', 'Company has been created successfully.');
            return redirect()->route('admin.companies.show', $company);
        } catch (\Exception $e) {
            Log::info($e);
            Session::flash('error', 'Error while tried to create company.');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('admin.companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        try {
            $company->update([
                'name' => $request->name,
                'email' => $request->email,
                'website' => $request->website,
            ]);

            if ($request->hasFile('logo')) {
                // delete logo
                $companyLogo = $company->logo_link;
                if ($companyLogo) {
                    $adminUrl = config('app.admin_url');
                    $path = str_replace($adminUrl, '', $companyLogo);
                    $file = public_path($path);

                    if (file_exists($file)) {
                        $deleteFile = unlink($file);
                        File::delete($file);

                        if (!$deleteFile) {
                            throw new Exception('Error while try to delete logo');
                        }

                        $company->update([
                            'logo_link' => null,
                        ]);
                    }
                }

                // upload logo
                $uploadPath = "assets/web/images/companies/$company->id/";
                $file      = $request->file('logo');
                $fileName = $request->logo->getClientOriginalName();
                $uploadSuccess = $file->move($uploadPath, $fileName); // uploading file to given path
                if (!$uploadSuccess) {
                    return redirect()->route("admin.companies.update")->with("failure", "Error when tried to save company logo.")->withInput();
                }

                $imageLink = $uploadPath . $fileName;
                $company->update([
                    'logo_link' => asset($imageLink),
                ]);
            };

            Session::flash('success', 'Company has been updated successfully.');
            return redirect()->route('admin.companies.show', $company);
        } catch (\Exception $e) {
            Log::info($e);
            Session::flash('error', 'Error while tried to update company. ('.$e->getMessage().')');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        if (!empty($company)) {

            // reset those employees under selected company
            if(count($company->employees) > 0) {
                foreach($company->employees as $employee) {
                    $employee->update([
                        'company_id' => null,
                    ]);
                }
            }

            $adminUrl = config('app.admin_url');

            if ($company->logo_link){
                $logoPath = str_replace($adminUrl, '', $company->logo_link);
                $profileImageFile = public_path($logoPath);

                if (file_exists($profileImageFile)) {
                    $profileImageDeleteFile = unlink($profileImageFile);
                    File::delete($company->logo_link);
                }

                $company->update([
                    'logo_link' => null,
                ]);
            }

            // delete company folder as well
            File::deleteDirectory(public_path('assets/web/images/companies/'. $company->id));

            $company->delete();

            Session::flash('success', 'Company has been deleted successfully.');
        }

        return redirect()->route('admin.companies.index');
    }

    public function deleteLogo(Company $company) {
        try{
            $companyLogo = $company->logo_link;
            if ($companyLogo) {
                $adminUrl = config('app.admin_url');
                $path = str_replace($adminUrl, '', $companyLogo);
                $file = public_path($path);

                if (file_exists($file)) {
                    $deleteFile = unlink($file);
                    File::delete($file);

                    if (!$deleteFile) {
                        return response()->json(['success' => false, 'message' => 'Error deleting file']);
                    }

                    $company->update([
                        'logo_link' => null,
                    ]);
                }
            }
            return response()->json(['success' => true, 'message' => null]);
        } catch (\Exception $e) {
            Log::info($e);
            Session::flash('error', 'Error deleting file. ('.$e->getMessage().')');
            return response()->json(['success' => false, 'message' => 'Error deleting file']);
        }
    }
}
