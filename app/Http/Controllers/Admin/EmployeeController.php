<?php

namespace App\Http\Controllers\Admin;

use Log;
use Session;
use Exception;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Company;
use App\Http\Requests\CreateEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::latest()->get();
        return view('admin.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::orderBy('name')->get();
        return view('admin.employees.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        try {
            $employee = Employee::create([
                'first_name'  => $request->first_name,
                'last_name'  => $request->last_name,
                'company_id' => $request->company,
                'email' => $request->email,
                'mobile_no'  => $request->mobile_no,
            ]);

            Session::flash('success', 'Employee has been created successfully.');
            return redirect()->route('admin.employees.show', $employee);
        } catch (\Exception $e) {
            Log::info($e);
            Session::flash('error', 'Error while tried to create employee.');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('admin.employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $companies = Company::orderBy('name')->get();
        return view('admin.employees.edit', compact('employee', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        try {
            $employee->update([
                'first_name'  => $request->first_name,
                'last_name'  => $request->last_name,
                'company_id' => $request->company,
                'email' => $request->email,
                'mobile_no'  => $request->mobile_no,
            ]);

            Session::flash('success', 'Employee has been updated successfully.');
            return redirect()->route('admin.employees.show', $employee);
        } catch (\Exception $e) {
            Log::info($e);
            Session::flash('error', 'Error while tried to update employee.');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        if (!empty($employee)) {
            $employee->delete();
            Session::flash('success', 'Employee has been deleted successfully.');
        }

        return redirect()->route('admin.employees.index');
    }
}
