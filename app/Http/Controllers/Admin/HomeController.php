<?php

namespace App\Http\Controllers\Admin;


// use App\Mail\Enquiry;
// use App\Mail\EnquiryUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{

    public function __construct()
    {
        
    }

    public function dashboard()
    {
        return view("admin.dashboard.index");
    }

}