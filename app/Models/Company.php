<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory;
    use SoftDeletes;

    // append extra custom attribute to model
    protected $fillable = array(
        'name',
        'logo_link',
        'email',
        'website',
    );

    public function employees() {
        return $this->hasMany(Employee::class, "company_id", "id");
    }

    public function getCompanyByEmail($email, $exceptCompanyId = 0) 
    {
        return $this->firstWhere([
            ['email', $email],
            ['id', '<>' , $exceptCompanyId],
        ]);
    }

    public function isExistingEmail($email, $exceptCompanyId = 0) 
    {
        if ($this->getCompanyByEmail($email, $exceptCompanyId)) {
            return true;
        }
        return false;
    }
}
