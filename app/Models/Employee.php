<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Company;

class Employee extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = array(
        'first_name',
        'last_name',
        'company_id',
        'email',
        'mobile_no',
    );

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getEmployeeByEmail($email, $exceptEmployeeId = 0) 
    {
        return $this->firstWhere([
            ['email', $email],
            ['id', '<>' , $exceptEmployeeId],
        ]);
    }

    public function isExistingEmail($email, $exceptEmployeeId = 0) 
    {
        if ($this->getEmployeeByEmail($email, $exceptEmployeeId)) {
            return true;
        }
        return false;
    }

    public function getEmployeeByMobileNo($mobileNo, $exceptEmployeeId = 0) 
    {
        return $this->firstWhere([
            ['mobile_no', $mobileNo],
            ['id', '<>' , $exceptEmployeeId],
        ]);
    }

    public function isExistingMobileNo($mobileNo, $exceptEmployeeId = 0) 
    {
        if ($this->getEmployeeByMobileNo($mobileNo, $exceptEmployeeId)) {
            return true;
        }
        return false;
    }
}
