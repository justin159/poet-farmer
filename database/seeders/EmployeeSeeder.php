<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class EmployeeSeeder extends Seeder
{
    protected $table = "employees";
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Drop foreign key constraint
        // DB::statement("ALTER TABLE $this->table DROP FOREIGN KEY employees_company_id_foreign");

        DB::table($this->table)->truncate();

        DB::table($this->table)->insert([
            'first_name' => 'Harper',
            'last_name' => 'Sullivan',
            'company_id' => 1,
            'email' => 'harper-sullivan@gmail.com',
            'mobile_no' => '0158932432',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Liam',
            'last_name' => 'Mitchell',
            'company_id' => 2,
            'email' => 'liam-mitchell@gmail.com',
            'mobile_no' => '038983273',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Ava',
            'last_name' => 'Reynolds',
            'company_id' => 3,
            'email' => 'aya-reynolds@gmail.com',
            'mobile_no' => '0183782332',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Noah',
            'last_name' => 'Turner',
            'company_id' => 4,
            'email' => 'noah-turner@gmail.com',
            'mobile_no' => '0127872860',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Sophia',
            'last_name' => 'Chandler',
            'company_id' => 5,
            'email' => 'sophia-chandler@gmail.com',
            'mobile_no' => '0219383873',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Jackson',
            'last_name' => 'Fields',
            'company_id' => 6,
            'email' => 'jackson-fields@gmail.com',
            'mobile_no' => '0197384373',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Olivia',
            'last_name' => 'Malone',
            'company_id' => 7,
            'email' => 'olicia-malone@gmail.com',
            'mobile_no' => '0133392842',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Lucas',
            'last_name' => 'Holloway',
            'company_id' => 8,
            'email' => 'lucas-holloway@gmail.com',
            'mobile_no' => '0185772323',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Isabella',
            'last_name' => 'Mercer',
            'company_id' => 9,
            'email' => 'isabella-mercer@gmail.com',
            'mobile_no' => '0389384711',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Ethan',
            'last_name' => 'Harper',
            'company_id' => 10,
            'email' => 'ethan-harper@gmail.com',
            'mobile_no' => '0189943434',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Mia',
            'last_name' => 'Stafford',
            'company_id' => 11,
            'email' => 'mia-stafford@gmail.com',
            'mobile_no' => '0377800221',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Mason',
            'last_name' => 'Donovan',
            'company_id' => 12,
            'email' => 'mason-donovan@gmail.com',
            'mobile_no' => '0158923844',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Amelia',
            'last_name' => 'Baxter',
            'company_id' => 13,
            'email' => 'amelia-baxter@gmail.com',
            'mobile_no' => '0127400330',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Aiden',
            'last_name' => 'Crosby',
            'company_id' => 14,
            'email' => 'aiden-crosby@gmail.com',
            'mobile_no' => '039937267',
        ]);

        DB::table($this->table)->insert([
            'first_name' => 'Harper',
            'last_name' => 'Sinclair',
            'company_id' => 15,
            'email' => 'harper-sinclair@gmail.com',
            'mobile_no' => '0198473833',
        ]);
    }
}
