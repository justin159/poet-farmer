<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CompanySeeder extends Seeder
{
    protected $table = "companies";
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->delete();
        
        DB::table($this->table)->insert([
            'name' => 'Lumina Labs',
            'email' => 'test@luminalabs.com',
            'logo_link' => null,
            'website' => 'http://www.luminalabs.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Catalyst Co.',
            'email' => 'test@catelyst-co.com',
            'logo_link' => null,
            'website' => 'http://www.catelyst-co.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Zenith Dynamics',
            'email' => 'test@zenith-dynamics.com',
            'logo_link' => null,
            'website' => 'http://www.zenith-dynamics.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Quantum Quasar',
            'email' => 'test@quantum-quasar.com',
            'logo_link' => null,
            'website' => 'http://www.quantum-quasar.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Nebula Innovate',
            'email' => 'test@nebula-innovate.com',
            'logo_link' => null,
            'website' => 'http://www.nebula-innovate.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Echelon Enterprises',
            'email' => 'test@echelon-ent.com',
            'logo_link' => null,
            'website' => 'http://www.echelon-ent.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Horizon Harmonics',
            'email' => 'test@horizon-harmonics.com',
            'logo_link' => null,
            'website' => 'http://www.horizon-harmonics.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Vertex Ventures',
            'email' => 'test@vertex-ventures.com',
            'logo_link' => null,
            'website' => 'http://www.vertex-ventures.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Fusion Frontier',
            'email' => 'test@fusion-frontier.com',
            'logo_link' => null,
            'website' => 'http://www.fusion-frontier.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Stratosphere Systems',
            'email' => 'test@stratopshere-sys.com',
            'logo_link' => null,
            'website' => 'http://www.stratopshere-sys.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Dynamo Dynamics',
            'email' => 'test@dynamo-dynamics.com',
            'logo_link' => null,
            'website' => 'http://www.dynamo-dynamics.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Celestial Synthesis',
            'email' => 'test@celestial-synthesis.com',
            'logo_link' => null,
            'website' => 'http://www.celestial-synthesis.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Nimbus Nexus',
            'email' => 'test@nimbus-nexus.com',
            'logo_link' => null,
            'website' => 'http://www.nimbus-nexus.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Quantum Quest',
            'email' => 'test@quantum-quest.com',
            'logo_link' => null,
            'website' => 'http://www.quantum-quest.com',
        ]);

        DB::table($this->table)->insert([
            'name' => 'Synergetic Solutions',
            'email' => 'test@synergetic-sol.com',
            'logo_link' => null,
            'website' => 'http://www.synergetic-sol.com',
        ]);
    }
}
