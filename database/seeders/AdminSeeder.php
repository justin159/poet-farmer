<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // BE CAREFUL, if run this seed will delete existing data

        DB::table('admins')->delete();

        $admins = array(
            array(
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('password'),
            ),
        );

        DB::table('admins')->insert($admins);
    }
}
